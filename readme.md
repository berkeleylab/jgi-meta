# jgi-meta

jgi-meta is a set of python tools for running the jgi metagenome, metatranscriptome assembly and reporting pipelines

## Installation

It requires python3
The jgi-meta repo can be forked with the collowing command:

```bash
git clone https://bfoster_jgi@bitbucket.org/berkeleylab/jgi-meta.git
```

## Usage

To run a test with toy data set you can run the following:
```python
./tests/meta_test.py;
bash tmp/cmds;
/bin/ls ./tmp/*/run.bash | xargs -i sbatch {} # for cori cluster submission
/bin/ls ./tmp/*/run.bash | xargs -i bash {} # to run locally
```

## Actual run

To run a production job:
```bash
pre_meta_assembly.py --templates -o setup.json /path/to/filtered/fastq.gz; bash run.bash
```


