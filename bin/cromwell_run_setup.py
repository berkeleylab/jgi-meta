#!/usr/bin/env python

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__),"..",'lib'))
import argparse
import json
import shutil
import re
import time
import report_utils as rep
import collections
import subprocess
import glob

'''
'''
if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

VERSION = "1.0.0"
def main():
    '''given a reads inputfile, and a wdl, copy reads to s3 and launch cromwell job 
    wait for completion and copy outputs back
    '''

    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-f", "--filtered", required=True, help="inputs filtered file\n")
    parser.add_argument("-w", "--wdl_aws", required=False, default=os.path.join(os.path.dirname(__file__),"..","templates","jgi_meta.aws.optimal.prod.wdl"))
    parser.add_argument("-l", "--wdl_local", required=False, default=os.path.join(os.path.dirname(__file__),"..","templates","jgi_meta.local.wdl"))
    parser.add_argument("-o", "--output_dir", required=False, default="./", help="outputdir for cromwell default basename.json\n")
    parser.add_argument("-c", "--cromwellhost", required=False,default="127.0.0.1:8000",  help="cromwell host to push job to\n")

    parser.add_argument("--finalize", required=False, default=False,action='store_true', help="add finalize step after complete")
    parser.add_argument("--force", required=False, default=False,action='store_true', help="force rerun")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()


    try:
        cmd = ['curl', '-Is', 'http://' + args.cromwellhost ]
        retcode = subprocess.check_output(cmd)
    except  e:
        print(e)
        sys.stderr.write(args.cromwellhost + " does not appear up \n")
        sys.exit(1)
    
    if not os.path.exists(args.filtered):
        sys.stderr.write(args.filtered + " does not exist\n")
        sys.exit(255)

    ts = time.time()
    output_json = args.output_dir + '/' + str(ts) + ".in.json"
    if os.path.exists(output_json):
        sys.stderr.write("Output json " + output_json + " already created.\n")
        sys.exit()

    pre_meta_assy_exe = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pre_meta_assembly.py")
    if not os.path.exists(pre_meta_assy_exe):
        sys.stderr.write(pre_meta_assy_exe, " Does not exits\n")
        sys.exit()
    
    pre_cmd = [pre_meta_assy_exe, args.filtered]
    metadata = json.loads(subprocess.Popen(pre_cmd, stdout=subprocess.PIPE).stdout.read())
    output_setup_json=os.path.join(args.output_dir , str(ts) + ".setup.json")
    with open(output_setup_json, "w") as f:f.write(json.dumps(metadata,indent=3))
    
    in_json = hasher()
    if len(metadata['inputs']) > 1:
        cat_file = os.path.join(args.output_dir, "combined." + str(time.time()) + ".fastq.gz")
        cat_cmd = "cat " + " ".join(metadata['inputs']) + " > " + cat_file
        out = subprocess.Popen(cat_cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        in_json['metadata']['input_files'] = metadata['inputs']
        in_json['jgi_meta.input_file'] = cat_file
        in_json['jgi_meta.uniquekmer']=str(10000000000) #default combined is 10b kmers
    elif len(metadata['inputs'])==1:
        in_json['metadata']['input_files'] = metadata['inputs']
        in_json['jgi_meta.input_file'] = metadata['inputs'][0]
        uniq=0
        try:
            metadata = rep.web_services('jamo', 'json', {"file_name":os.path.basename(args.filtered)})[0]
            su = metadata['metadata']['seq_unit_name'][0]
            dump = rep.web_services('rqc', 'su', su)
            uniq = dump['stats']['filter:kmer_depth_total_unique_kmer_count']
        except:
            sys.stderr.write("Cant get filter:kmer_depth_total_unique_kmer_count for su")
            uniq=10000000000 #default combined is 10b kmers
        in_json['jgi_meta.uniquekmer']=str(uniq)                          
    else:
        sys.stderr.write("Error finding input files with " + pre_cmd)

    in_json['metadata']['wdl'] = args.wdl_aws
    in_json['metadata']['run_dir'] = args.output_dir
    in_json['metadata']['setup_json'] = output_setup_json
    with open(output_json,"w")as f:f.write(json.dumps(in_json,indent=3))

    aws_bash = output_json + ".aws.bash"
    with open(aws_bash,"w")as f:
        finalize = "--finalize" if args.finalize else ""
        cmd = os.path.join(os.path.dirname(__file__),"cromwell_run.py")
        cmd += " -j " + output_json + " -c " + args.cromwellhost +  " -w " + args.wdl_aws + " " +  finalize + " --verbose 1>| " + output_json + ".aws.o 2>| " + output_json + ".aws.e"
        f.write('#!/usr/bin/env bash' + "\n\n")
        f.write("ulimit -c 0\numask 002\nset -eo pipefail\n\n")
        f.write("cd " + os.path.dirname(os.path.realpath(output_json)) + "\n\n")
        f.write(cmd + "\n")
    os.chmod(aws_bash, 0o755)
    print(aws_bash)
    local_bash = output_json + ".local.bash"
    with open(local_bash,"w")as f:
        f.write('#!/usr/bin/env bash' + "\n\n")
        f.write("ulimit -c 0\numask 002\nset -eo pipefail\n\n")
        f.write("cd " + os.path.dirname(os.path.realpath(output_json)) + "\n\n")
        cromwell_jar = ""
        try:
            cromwell_jar = glob.glob(os.path.join(os.path.dirname(__file__),"cromwell-*.jar"))[0]
        except:
            pass
        if not cromwell_jar:
            f.write('if ! ls cromwell-*.jar 1> /dev/null 2>&1 ; then curl -s https://api.github.com/repos/broadinstitute/cromwell/releases/latest | jq -r .assets[].browser_download_url | grep -v wom | xargs -i curl --silent -O -L {}; fi' + "\n")
            f.write('java -jar cromwell*.jar run --inputs ' + output_json + ' --metadata-output ' + output_json + '.metadata.json ' + args.wdl_local + ' 1>| ' + output_json + ".local.o 2>| " + output_json + ".local.e")
        else:
            f.write('java -jar ' + cromwell_jar + ' run --inputs ' + output_json + ' --metadata-output ' + output_json + '.metadata.json ' + args.wdl_local + ' 1>| ' + output_json + ".local.o 2>| " + output_json + ".local.e")
        
        f.write("\n\n")

        f.write("\n")
    os.chmod(local_bash, 0o755)
    sys.exit()
    
def hasher():
    '''supports perl style auto-vivification of arbitry depth dicts'''
    return collections.defaultdict(hasher)

if __name__ == '__main__':
    main()
