#!/usr/bin/env python
import sys, os
import json
import argparse
import subprocess
import glob
import textwrap
import time
import collections

RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"

path = os.path.realpath(__file__)
sys.path.insert(0, os.path.dirname(path))
sys.path.insert(0, os.path.join(os.path.dirname(path),"..",'lib'))

import jgi_mga_utils
import jgi_meta_project_utils
import qcutils

VERSION = "1.0.0"
'''
-todo: 
       -add another oo module (currently post) for creation of Tables and generating rqc-pdf - jgi_meta_SIGS_rqc.py (assy_metadata, aln_metadata, 
       -add metadata.json creation and md/report validation
       -add Metadata.json class in tool_info_utils.py as module instead of system call

'''

'''
jgi_mga_report.py
given a metatranscriptome mapping alignment run folder and metadata file,
produce README.txt and README.pdf in that dir

depends on the following files:
    files = dict()

metadata file has the following format:
{
   "inputs": [
      "/global/dna/shared/rqc/filtered_seq_unit/00/01/20/03/12003.1.232774.GATCAG.filter-MTF.fastq.gz"
   ],
   "metadata": {
      "library_name": [
         "CBTXG"
      ],
      "analysis_task_id": [
         206031
      ],
      "analysis_project_id": [
         1159907
      ],
      "sequencing_project_id": [
         1159930
      ],
      "seq_unit_name": [
         "12003.1.232774.GATCAG.fastq.gz"
      ]
   }
}

20171109
   bf initial version
'''

def main():
    '''main'''
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-i", "--input_dir", required=True, help="Analysis dir.\n")
    parser.add_argument("-d", "--debug", required=False, default=False,action='store_true', help="debug.\n")
    parser.add_argument("-j", "--json", required=True, help="setup json file.\n")
    args = parser.parse_args()

    if not os.path.exists(args.json):
        sys.exit("Can't find " + args.json)

    with open(args.json, "r") as f:
        meta = json.loads(f.read())

    #gather files
    paths = dict()
    paths['bfc_dir']=glob.glob(os.path.join(args.input_dir,"bfc"))[0]
    paths['bfc_stats']=glob.glob(os.path.join(args.input_dir,"readstats_bfc","readlen.txt"))[0]
    paths['assembly_dir']=glob.glob(os.path.join(args.input_dir,"assy"))[0]
    paths['assembly_stats']=glob.glob(os.path.join(args.input_dir,"*","*.scaffolds.fasta.stats.txt"))[0]
    paths['alignment_dir']=glob.glob(os.path.join(args.input_dir,"readMappingPairs"))[0]
    paths['alignment_stats']=glob.glob(os.path.join(args.input_dir,"readMappingPairs","command.bash.e"))[0]

    report_text = ""
    if len(meta['metadata']['sequencing_project_id']) >0:
        meta = add_project_info(meta)
        subtitle =  meta['metadata']['sequencing_project_name']  + ", ASSEMBLY_DATE=" +  time.strftime("%Y%m%d",time.localtime())
        report_text += "JGI assembly of: " + subtitle + "\n\n"
        report_text += "Proposal Name: " + "\n".join(textwrap.wrap(meta['metadata']['proposal_title'] + "\n", width=80, subsequent_indent='\t\t')) + "\n"
        report_text += "Proposal ID: " + str(meta['metadata']['proposal_id']) + "\n"
        report_text += "Sequencing Project ID(s): " + ", ".join([str(x) for x in meta['metadata']['sequencing_project_id']]) + "\n"
        if len(meta['metadata']['analysis_project_id']) >0:
            assembly_ap=meta['metadata']['analysis_project_id'][0]
            report_text += "Analysis Project/Task ID: " + str(meta['metadata']['analysis_project_id'][0]) + "/" + str(meta['metadata']['analysis_task_id'][0]) +  "\n"
        if meta['metadata']['pi_name']:
            report_text += "Principal Investigator: " + meta['metadata']['pi_name']  + " - " + meta['metadata'].get('pi_email',"") + "\n"

    report_text += "\n"

    report_text += "Input Data:\n"

    raw_count = int()
    filtered_count = int()

    for inputfile in meta['inputs']:
        if fileisfiltered(inputfile):
            lib_info = filtered_to_libinfo(inputfile)
            report_text += "\t" + "\n\t".join(lib_info['readme'].rstrip().split("\n")) + "\n\n"
            raw_count += lib_info['lib']['raw_count']
            filtered_count += lib_info['lib']['filtered_count']
        else:
            report_text += "\t" + inputfile + "\n\n"

    if raw_count and filtered_count:
        report_text += "Read Pre-processing:\n"
        report_text += "\tThe number of raw input reads is: " + str(raw_count) + "\n"
        report_text += "\tThe number of filtered input reads is: " + str(filtered_count) + "\n\n"


    bfc_metadata = get_tool_metadata('bfc',paths['bfc_dir'], paths['bfc_stats'])
    report_text += "Read Correction Stats:\n" + "\t" + "\n\t".join(bfc_metadata['stats'].rstrip().split("\n")) + "\n\n";
    
    assembly_metadata = get_tool_metadata('metaspades',paths['assembly_dir'], paths['assembly_stats'])
    report_text += "Assembly Stats:\n" + "\t" + "\n\t".join(assembly_metadata['stats'].rstrip().split("\n")) + "\n\n";

    alignment_metadata = get_tool_metadata('bbmap',paths['alignment_dir'], paths['alignment_stats'])
    report_text += "Alignment Stats:\n" + "\t" + "\n\t".join(alignment_metadata['stats'].rstrip().split("\n")) + "\n\n";

    #Methods section
    #report_text += "Methods:\n\n"
    #report_text +=  "\n".join(textwrap.wrap(bfc_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n"
    #report_text += "\n".join(textwrap.wrap(assembly_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n";
    #report_text += "\n".join(textwrap.wrap(alignment_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n";

    report_text += "Methods:\n\n"
    bfc =  "\n".join(textwrap.wrap(bfc_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n"
    report_text += bfc.replace("__REF__","1")
    assy = "\n".join(textwrap.wrap(assembly_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n";
    report_text += assy.replace("__REF__","2")
    aln = "\n".join(textwrap.wrap(alignment_metadata['readme'].rstrip(), width=90, subsequent_indent='')) + "\n\n";
    report_text += aln.replace("__REF__","3")
    
    report_text += "\n\n"
    report_text += "(1) " + "\n".join(textwrap.wrap(bfc_metadata['reference'].rstrip(), width=90, subsequent_indent='')) + "\n"
    report_text += "(2) " + "\n".join(textwrap.wrap(assembly_metadata['reference'].rstrip(), width=90, subsequent_indent='')) + "\n"
    report_text += "(3) " + "\n".join(textwrap.wrap(alignment_metadata['reference'].rstrip(), width=90, subsequent_indent='')) + "\n\n"
                                            
    cmd = "jgi_meta_run.py --version"
    pipeline_version = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stderr.readlines()[0].rstrip()
    report_text += "The version of the processing pipeline is " + pipeline_version + "\n\n"
    report_text += "If you have any questions, please contact the JGI project manager.\n"
#    print report_text
    covfile =glob.glob(os.path.join(paths['alignment_dir'],"covstats.txt"))
    if len(covfile)==1:
        covfile = covfile[0]
    (cov, gccov, gc) = generate_graphics(covfile,os.path.join(args.input_dir, "report"), 'report')
    (txtout, htmlout, pdfout) = generate_pdf(txt=report_text, graphics=[cov, gccov, gc], outdir=os.path.join(args.input_dir, "report"), prefix='README')
    return

def get_tool_metadata(tool, analysis_dir, analysis_stats_file):
    if os.path.exists(analysis_dir) and os.path.exists(analysis_stats_file):
        cmd = os.path.join(os.path.dirname(path),"tool_info_utils.py") + " --tool " + tool + " -d " + analysis_dir + " --statsfile " + analysis_stats_file
        metadata = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read()
        assembly_metadata = json.loads(metadata)
    return assembly_metadata
    
def add_project_info(meta):
    '''takes metadata section and adds proposal and pi information'''
    sequencing_project_ids = meta['metadata']['sequencing_project_id']
    sequencing_project_name = ", ".join(list(set([qcutils.get_proj_info(x)[0][2] for x in sequencing_project_ids])))
    proposal_id, proposal_title, pi_name, pi_email = qcutils.get_proposal_info(sequencing_project_ids[0], verbose=False)
    project_info = { 
        'sequencing_project_name'   : sequencing_project_name,
        'proposal_title'            : proposal_title,
        'proposal_id'               : proposal_id,
        'pi_name'                   : pi_name,
        'pi_email'                  : pi_email }
    meta['metadata'].update(project_info)
    return meta
    
def fileisfiltered(filename):
    filtered = False
    filename = os.path.basename(filename)
    if filename.find("META")==-1:
        return False
    try:
        jamo = jgi_mga_utils.web_services('jamo', {'file_name':os.path.basename(filename)})
        if jamo[-1]['metadata']['fastq_type'] == 'filtered':
            filtered = True
    except:
        filtered = False
    return filtered

    
def generate_pdf(**kwargs):
#        kwargs['txt']
#        kwargs['graphics'] list
#        kwargs['outdir']
#        kwargs['prefix']

        txtout =  os.path.join(kwargs['outdir'],kwargs['prefix'] +'.txt')
        htmlout = os.path.join(kwargs['outdir'],kwargs['prefix'] +'.html')
        pdfout = os.path.join(kwargs['outdir'],kwargs['prefix'] +'.pdf')
        with open(txtout,"w") as f:
            f.write(kwargs['txt'] + "\n")

        with open(htmlout,"w") as f:
            f.write('''
        <html>
            <head>
            </head>
            <body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#ff0000" alink="#ffff00">
            <pre>
            ''')
            f.write(kwargs['txt'] + "\n")
            f.write("        </pre>\n")
            for image in kwargs['graphics']:
                f.write('<img src="' + image + '"/>'  + "\n")
            f.write("            </body>\n")
            f.write("</html>\n")
        cmd = "wkhtmltopdf " + htmlout + " " + pdfout + ' 1> ' + pdfout + '.o 2> ' + pdfout + '.e'
        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        return (txtout,htmlout, pdfout)

def generate_graphics(coverage_file, out_dir, prefix):
    #graphics routine from LANL
    rscript = os.path.join(out_dir,prefix + ".R")
    outstub =  os.path.join(out_dir,prefix)
    outcov = outstub + '_avg_fold_vs_len.bmp'
    outgccov = outstub + '_gc_vs_avg_fold.bmp'
    outgc = outstub + '_gc.bmp'
    with open(rscript,"w") as f:
        f.write('a<-read.table(file="' + coverage_file + '",header=TRUE,comment="",sep="")' + "\n")
        f.write('bitmap("' + outcov + '")' + "\n")
        f.write('plot(a$Length,a$Avg_fold,pch=8,xlab="Contigs Length (bp)",ylab="Average coverage fold (x)",cex=0.7, main="Contigs average fold coverage vs. Contigs Length",log="y")' + "\n")
        f.write('tmp<-dev.off();')
        f.write('bitmap("' + outgccov + '");' + "\n")
        f.write('par(mar = c(5, 5, 5, 5), xpd=TRUE, cex.main=1.5, cex.lab=1.2);' + "\n")
        f.write('plot(a$Avg_fold,a$Ref_GC,ylab="GC (%)",xlab="Average coverage fold (x)", main="Contigs average fold coverage vs. GC",pch=20,col="blue",cex=log(a$Length/mean(a$Length)),log="x");tmp<-dev.off();' + "\n" )
        f.write('bitmap("' + outgc + '");hist(subset(a,a$Length>0)$Ref_GC*100,seq(from=0,to=102,by=1.5),main="GC Histogram for contigs",ylab="# of contigs",xlab="GC (%)");dev.off();quit();' + "\n")
    cmd = ' cat ' + rscript + ' | R --vanilla --slave --silent 1>| ' + rscript + '.o' + ' 2>| ' + rscript + '.e'
    out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    
    return (outcov, outgccov, outgc)

def filtered_to_libinfo(inputfile):
    '''
    filtered_to_libinfo
    input = input file
    output = text of library info for reports
    '''
    return_dict = hasher()
    info = ""
    filename = os.path.basename(inputfile)
    project = jgi_meta_project_utils.ap_project()
    raw = project.filtered_to_raw(filename)
    jamo = jgi_mga_utils.web_services('jamo', {'file_name':os.path.basename(raw)})
    jamo_filtered = jgi_mga_utils.web_services('jamo', {'file_name':os.path.basename(filename)})    
    lib = dict()
    if "sow_segment" in list(jamo[0]["metadata"].keys()) and len(jamo)==1:
        #instrument data
        if "instrument_type" in list(jamo[0]["metadata"]["sow_segment"].keys()):
            lib["instrument_type"] = jamo[0]["metadata"]["sow_segment"].get("instrument_type","")
        elif "sequencer_model" in list(jamo[0]["metadata"]["sow_segment"].keys()):
            lib["instrument_type"] = jamo[0]["metadata"]["sow_segment"]["sequencer_model"]
        elif  "physical_run" in list(jamo[0]["metadata"].keys()) and "instrument_type" in jamo[0]["metadata"]["physical_run"]:
            lib["instrument_type"] = jamo[0]["metadata"]["physical_run"]["instrument_type"]
        else:
            lib["instrument_type"] = "NA"

        #Platform data
        if "platform_name" in list(jamo[0]["metadata"]["sow_segment"].keys()):
            lib["platform_name"] = jamo[0]["metadata"]["sow_segment"]["platform_name"]
        elif "platform" in list(jamo[0]["metadata"]["sow_segment"].keys()):
            lib["platform_name"] = jamo[0]["metadata"]["sow_segment"]["platform"]
        elif "physical_run" in list(jamo[0]["metadata"].keys()) and "platform_name" in jamo[0]["metadata"]["physical_run"]:
            lib["platform_name"] = jamo[0]["metadata"]["physical_run"]["platform_name"]
        else:
            lib["platform_name"] = "NA"
            
        #other lib information
        lib["library_protocol"] = jamo[0]["metadata"]["sow_segment"].get("library_protocol","")
        lib["target_fragment_size_bp"] = jamo[0]["metadata"]["sow_segment"].get("target_fragment_size_bp","")
        lib["raw_count"] = int(jamo_filtered[0]["metadata"].get('input_read_count', ""))
        lib["filtered_count"] = int(jamo_filtered[0]["metadata"].get('filter_reads_count', ""))
        
        info += "Library: " + jamo[0]['metadata']['library_name'] + "\n"
        info += "Platform: " + lib['platform_name'] + " " + lib['instrument_type'] + " " + lib['library_protocol'] +  " " + str(lib["target_fragment_size_bp"]) +  " bp fragment\n" ;
        info += "Filtered Data: " + os.path.basename(inputfile) + "\n"
        info += "Filtered Read Count: " + str(lib["filtered_count"]) + "\n"
        info += "Raw Data: " + os.path.basename(raw) +  "\n"
        info += "Raw Read Count: " + str(lib["raw_count"]) + "\n"
        return_dict['readme'] = info
        return_dict['lib'] = lib
    return return_dict

def hasher():
    '''supports perl style auto-vivification of arbitry depth dicts'''
    return collections.defaultdict(hasher)

                        #### END FUNCTIONS ####
if __name__ == "__main__":
    main()
